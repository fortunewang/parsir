use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_chunk() {
    let s = &mut CharStream::new("hello world".chars());
    assert_eq!(lexer::chunk("hello".chars()).try_parse(s), Ok("hello".to_string()));
}

