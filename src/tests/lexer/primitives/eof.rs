use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_eof() {
    let s = &mut CharStream::new("a".chars());
    assert!(lexer::eof().try_parse(s).is_err());
    assert_eq!(lexer::any_single().try_parse(s), Ok('a'));
    assert_eq!(lexer::eof().try_parse(s), Ok(()));
}
