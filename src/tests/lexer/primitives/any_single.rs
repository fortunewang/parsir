use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_any_single() {
    let s = &mut CharStream::new("abc".chars());
    assert_eq!(lexer::any_single().try_parse(s), Ok('a'));
}

