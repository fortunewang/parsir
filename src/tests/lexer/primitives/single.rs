use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_single() {
    let s = &mut CharStream::new("abc".chars());
    assert!(lexer::single('b').try_parse(s).is_err());
    assert_eq!(lexer::single('a').try_parse(s), Ok('a'));
}

