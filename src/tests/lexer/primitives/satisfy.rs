use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_satisfy() {
    let s = &mut CharStream::new("a".chars());
    assert!(lexer::satisfy(|c: char| c.is_ascii_uppercase()).try_parse(s).is_err());
    assert_eq!(lexer::satisfy(|c: char| c.is_ascii_alphabetic()).try_parse(s), Ok('a'));
}

