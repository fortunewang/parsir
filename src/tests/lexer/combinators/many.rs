use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_many() {
    let s = &mut CharStream::new("aaaab".chars());
    let parser = lexer::many(lexer::single('a'));
    assert_eq!(parser.try_parse(s), Ok("aaaa".to_string()));
}