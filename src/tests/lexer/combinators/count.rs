use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_count() {
    let s = &mut CharStream::new("aaaab".chars());
    let parser = lexer::count(5, lexer::single('a'));
    assert!((parser.try_parse(s) as Result<String, _>).is_err());
    let parser = lexer::count(4, lexer::single('a'));
    assert_eq!(parser.try_parse(s), Ok("aaaa".to_string()));
}