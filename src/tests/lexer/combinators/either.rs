use crate::Parser;
use crate::lexer;
use crate::implements::CharStream;

#[test]
fn test_either() {
    let s = &mut CharStream::new("#f".chars());
    let parser = lexer::either(
        lexer::chunk("#t".chars()),
        lexer::chunk("#f".chars())
    );
    assert_eq!(parser.try_parse(s), Ok("#f".to_string()))
}
