use crate::Stream;
use crate::implements::{CharStream, U8Stream};

#[test]
fn test_char_stream() {
    let s = &mut CharStream::new("和".chars());
    let context = s.track();
    assert_eq!(s.read(), Some('和'));
    assert_eq!(s.read(), None);
    s.rollback(context);
    assert_eq!(s.read(), Some('和'));
}

#[test]
fn test_u8_stream() {
    let s = &mut U8Stream::new("和".bytes());
    let context = s.track();
    assert_eq!(s.read(), Some(0xe5));
    assert_eq!(s.read(), Some(0x92));
    assert_eq!(s.read(), Some(0x8c));
    assert_eq!(s.read(), None);
    s.rollback(context);
    assert_eq!(s.read(), Some(0xe5));
}