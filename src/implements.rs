mod buffered;

mod char;
pub use self::char::CharStream;

mod u8;
pub use self::u8::U8Stream;
