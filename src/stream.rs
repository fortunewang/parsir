use std::fmt::Debug;

pub trait Token: Debug + Eq + Copy {}

pub trait Stream<T> where T: Token {
    type Context;
    type State;
    fn read(&mut self) -> Option<T>;
    fn track(&mut self) -> Self::Context;
    fn commit(&mut self, c: Self::Context);
    fn rollback(&mut self, c: Self::Context);
    fn state(&self) -> Self::State;
}
