
use crate::stream::{Token, Stream};

// pub struct LabeledParser<P> {
//     label: String,
//     parser: P,
// }

pub trait Parser<R, T> where T: Token {
    // fn expecting(&self) -> Vec<String> { Vec::new() }

    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<R, String>;

    fn try_parse<S: Stream<T>>(&self, s: &mut S) -> Result<R, String> {
        let context = s.track();
        let res = self.parse(s);
        if res.is_ok() {
            s.commit(context);
        } else {
            s.rollback(context);
        }
        res
    }

    fn peek<S: Stream<T>>(&self, s: &mut S) -> Result<R, String> {
        let context = s.track();
        let res = self.parse(s);
        s.rollback(context);
        res
    }

    // fn label(self, name: &str) -> LabeledParser<Self> where Self: Sized {
    //     LabeledParser {
    //         label: name.to_string(),
    //         parser: self,
    //     }
    // }
}

// impl<R, T, P> Parser<R, T> for LabeledParser<P> where T: Token, P: Parser<R, T> {
//     #[inline]
//     fn expecting(&self) -> Vec<String> { vec![self.label.clone()] }
//     #[inline]
//     fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<R, String> { self.parser.parse(s) }
// }

// impl <R, T, F> Parser<R, T> for F
// where
//     T: Token,
//     F: Fn(&mut dyn Stream<T>) -> Result<R, String>
// {
//     fn parse<S: Stream<T>>(&self, stream: &mut S) -> Result<R, String> {
//         (self)(stream)
//     }
// }

