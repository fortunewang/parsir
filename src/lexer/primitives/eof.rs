use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct EOFParser<T> where T: Token {
    _t: std::marker::PhantomData<T>,
}

impl<T> Parser<(), T> for EOFParser<T> where T: Token {
    // fn expecting(&self) -> Vec<String> { vec!["end of input".to_string()] }

    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<(), String> {
        if s.read().is_some() {
            Err("expect end-of-input".to_string())
        } else {
            Ok(())
        }
    }
}

pub fn eof<T>() -> EOFParser<T> where T: Token {
    EOFParser {
        _t: std::marker::PhantomData,
    }
}
