use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct AnySingleParser<T> where T: Token {
    _t: std::marker::PhantomData<T>,
}

impl<T> Parser<T, T> for AnySingleParser<T> where T: Token {
    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<T, String> {
        s.read().ok_or_else(|| "stream is empty".to_string())
    }
}

pub fn any_single<T>() -> AnySingleParser<T> where T: Token {
    AnySingleParser { _t: std::marker::PhantomData }
}
