use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct SatisfyParser<T, F> where T: Token, F: Fn(T) -> bool {
    f: F,
    _t: std::marker::PhantomData<T>,
}

impl<T, F> Parser<T, T> for SatisfyParser<T, F> where T: Token, F: Fn(T) -> bool {
    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<T, String> {
        match s.read() {
            Some(t) if (self.f)(t) => Ok(t),
            _ => Err("unexpected token".to_string()),
        }
    }
}

pub fn satisfy<T, F>(f: F) -> SatisfyParser<T, F> where T: Token, F: Fn(T) -> bool {
    SatisfyParser { f, _t: std::marker::PhantomData, }
}
