use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct SingleParser<T> where T: Token {
    token: T,
}

impl<T> Parser<T, T> for SingleParser<T> where T: Token {
    // fn expecting(&self) -> Vec<String> { vec![format!("{:?}", self.token)] }

    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<T, String> {
        match s.read() {
            Some(t) if t == self.token => Ok(t),
            _ => Err("unexpected token".to_string()),
        }
    }
}

pub fn single<T>(token: T) -> SingleParser<T> where T: Token {
    SingleParser { token }
}
