use std::iter::FromIterator;
use crate::stream::{Token, Stream};
use crate::parser::Parser;
use super::super::combinators;
use super::super::primitives;

pub struct ChunkParser<T> where T: Token {
    chunk: Vec<T>,
    parser: combinators::CountParser<T, T, primitives::AnySingleParser<T>>,
}

impl<M, T> Parser<M, T> for ChunkParser<T>
where T: Token, M: FromIterator<T>
{
    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<M, String> {
        let chunk: Vec<_> = self.parser.parse(s)?;
        if chunk == self.chunk {
            Ok(M::from_iter(chunk.into_iter()))
        } else {
            Err("unexpected token".to_string())
        }
    }
}

pub fn chunk<T, TS>(source: TS) -> ChunkParser<T>
where T: Token, TS: IntoIterator<Item = T>
{
    let chunk = Vec::from_iter(source);
    let parser = combinators::count(chunk.len(), primitives::any_single());
    ChunkParser { chunk, parser }
}
