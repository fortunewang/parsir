use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct EitherParser<R, T, P1, P2>
where
    T: Token,
    P1: Parser<R, T>,
    P2: Parser<R, T>,
{
    p1: P1,
    p2: P2,
    _r: std::marker::PhantomData<R>,
    _t: std::marker::PhantomData<T>,
}

impl<R, T, P1, P2> Parser<R, T> for EitherParser<R, T, P1, P2>
where
    T: Token,
    P1: Parser<R, T>,
    P2: Parser<R, T>,
{
    // fn expecting(&self) -> Vec<String> {
    //     let mut tokens = self.p1.expecting();
    //     tokens.extend(self.p2.expecting());
    //     tokens
    // }

    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<R, String> {        
        self.p1.try_parse(s).or_else(|_| self.p2.parse(s))
    }
}

pub fn either<R, T, P1, P2>(p1: P1, p2: P2) -> EitherParser<R, T, P1, P2>
where
    T: Token,
    P1: Parser<R, T>,
    P2: Parser<R, T>,
{
    EitherParser {
        p1, p2,
        _r: std::marker::PhantomData,
        _t: std::marker::PhantomData,
    }
}