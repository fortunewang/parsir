
use std::iter::FromIterator;
use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct CountParser<R, T, P> where
    T: Token,
    P: Parser<R, T>,
{
    count: usize,
    p: P,
    _r: std::marker::PhantomData<R>,
    _t: std::marker::PhantomData<T>,
}

impl<R, M, T, P> Parser<M, T> for CountParser<R, T, P>
where
    M: FromIterator<R>,
    T: Token,
    P: Parser<R, T>,
{
    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<M, String> {
        if self.count == 0 {
            return Ok(M::from_iter(Vec::new().into_iter()));
        }
        let mut rs = Vec::new();
        while let Ok(r) = self.p.try_parse(s) {
            rs.push(r);
            if rs.len() >= self.count { break }
        }
        if rs.len() >= self.count {
            Ok(M::from_iter(rs.into_iter()))
        } else {
            Err("cannot match exact times".to_string())
        }
    }
}

pub fn count<R, T, P>(count: usize, p: P) -> CountParser<R, T, P>
where T: Token, P: Parser<R, T>,
{
    CountParser {
        count, p,
        _r: std::marker::PhantomData,
        _t: std::marker::PhantomData,
    }
}
