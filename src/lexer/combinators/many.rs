
use std::iter::FromIterator;
use crate::stream::{Token, Stream};
use crate::parser::Parser;

pub struct ManyParser<R, T, P> where
    T: Token,
    P: Parser<R, T>,
{
    p: P,
    _r: std::marker::PhantomData<R>,
    _t: std::marker::PhantomData<T>,
}

impl<R, M, T, P> Parser<M, T> for ManyParser<R, T, P>
where
    M: FromIterator<R>,
    T: Token,
    P: Parser<R, T>,
{
    // fn expecting(&self) -> Vec<String> { self.p.expecting() }

    fn parse<S: Stream<T>>(&self, s: &mut S) -> Result<M, String> {
        let mut rs = Vec::new();
        loop {
            if let Ok(r) = self.p.try_parse(s) {
                rs.push(r);
            } else {
                break;
            }
        }
        Ok(M::from_iter(rs.into_iter()))
    }
}

pub fn many<R, T, P>(p: P) -> ManyParser<R, T, P>
where T: Token, P: Parser<R, T>,
{
    ManyParser {
        p,
        _r: std::marker::PhantomData,
        _t: std::marker::PhantomData,
    }
}
