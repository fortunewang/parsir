use crate::stream::{Token, Stream};
use super::buffered::Buffered;

impl Token for char {}

#[derive(Clone)]
pub struct State {
    line: usize,
    column: usize,
}

pub struct Context<Iter> where Iter: Iterator<Item = char> {
    state: State,
    inner: <Buffered<char, Iter> as Stream<char>>::Context,
}

pub struct CharStream<Iter> where Iter: Iterator<Item = char> {
    inner: Buffered<char, Iter>,
    current_state: State,
}

impl<Iter> CharStream<Iter> where Iter: Iterator<Item = char> {
    pub fn new(iter: Iter) -> Self {
        Self {
            inner: Buffered::new(iter),
            current_state: State { line: 0, column: 0 },
        }
    }
}

impl<Iter> Stream<char> for CharStream<Iter> where Iter: Iterator<Item = char> {
    type Context = Context<Iter>;
    type State = State;

    fn read(&mut self) -> Option<char> {
        let res = self.inner.read();
        if let Some(c) = res {
            if c == '\r' || c == '\n' {
                self.current_state.line += 1;
                self.current_state.column = 0;
            } else {
                self.current_state.column += 1;
            }
        }
        res
    }

    fn track(&mut self) -> Context<Iter> {
        Context {
            state: self.current_state.clone(),
            inner: self.inner.track(),
        }
    }

    fn commit(&mut self, c: Context<Iter>) {
        self.inner.commit(c.inner)
    }

    fn rollback(&mut self, c: Context<Iter>) {
        self.current_state = c.state;
        self.inner.rollback(c.inner)
    }

    fn state(&self) -> State { self.current_state.clone() }
}