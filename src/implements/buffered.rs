use crate::stream::{Token, Stream};
use std::collections::VecDeque;

pub struct Buffered<T, Iter> where T: Token, Iter: Iterator<Item = T> {
    input: Iter,
    buffer: VecDeque<T>,
    offset: usize,
    tracked: usize,
}

impl<T, Iter> Buffered<T, Iter> where T: Token, Iter: Iterator<Item = T> {
    pub fn new(input: Iter) -> Self {
        Self {
            input: input,
            buffer: VecDeque::new(),
            offset: 0,
            tracked: 0,
        }
    }
}

impl<T, Iter> Stream<T> for Buffered<T, Iter> where T: Token, Iter: Iterator<Item = T> {
    type Context = usize;
    type State = ();

    fn track(&mut self) -> usize {
        self.tracked += 1;
        self.offset
    }

    fn commit(&mut self, pos: usize) {
        self.tracked -= 1;
        if self.tracked == 0 && pos > 0 {
            self.buffer = self.buffer.drain(.. pos).collect();
            self.offset -= pos;
        }
    }

    fn rollback(&mut self, pos: usize) {
        self.tracked -= 1;
        self.offset = pos;
    }

    fn read(&mut self) -> Option<T> {
        if self.tracked > 0 {
            if let Some(c) = self.buffer.iter().skip(self.offset).next() {
                self.offset += 1;
                return Some(*c);
            }
            if let Some(c) = self.input.next() {
                self.buffer.push_back(c);
                self.offset += 1;
                return Some(c);
            }
        } else {
            // offset should be 0 if not tracked
            if let Some(c) = self.buffer.pop_front() {
                return Some(c);
            }
            if let Some(c) = self.input.next() {
                return Some(c);
            }
        }
        None
    }

    fn state(&self) -> () {}
}
