use crate::stream::Token;
use super::buffered::Buffered;

impl Token for u8 {}
pub type U8Stream<Iter> = Buffered<u8, Iter>;
