use parsir::{Stream, Parser};
use parsir::lexer;

#[derive(Debug, PartialEq)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

pub struct HexParser {}

impl HexParser {
    pub fn new() -> Self { Self { } }
}

impl Parser<u8, char> for HexParser {

    fn parse<S: Stream<char>>(&self, s: &mut S) -> Result<u8, String> {
        let literal: String = lexer::count(2, lexer::satisfy(|c: char| c.is_digit(16))).parse(s)?;
        u8::from_str_radix(&literal, 16).map_err(|e| e.to_string())
    }
}

pub struct HexColorParser {
    hex: HexParser,
}

impl HexColorParser {
    pub fn new() -> Self { Self { hex: HexParser::new() } }
}

impl Parser<Color, char> for HexColorParser {

    fn parse<S: Stream<char>>(&self, s: &mut S) -> Result<Color, String> {
        lexer::single('#').try_parse(s)?;
        let red = self.hex.try_parse(s)?;
        let green = self.hex.try_parse(s)?;
        let blue = self.hex.try_parse(s)?;
        lexer::eof().try_parse(s)?;
        Ok(Color { red, green, blue })
    }
}

#[cfg(test)]
mod tests {
    use parsir::Parser;
    use parsir::implements::CharStream;
    use super::{Color, HexColorParser};

    #[test]
    fn it_works() {
        let s = &mut CharStream::new("#2F14DF".chars());
        let parser = HexColorParser::new();
        assert_eq!(parser.parse(s), Ok(Color {
            red: 47,
            green: 20,
            blue: 223,
        }));
    }
}
